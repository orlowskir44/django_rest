from django.contrib import admin
from .models import Currency, CurrencyPair


admin.site.register(Currency)


@admin.register(CurrencyPair)
class CurrencyPairAdmin(admin.ModelAdmin):
    list_display = ['currency_pair', 'exchange_rate', 'created_at']  # 'id'?
    list_filter = ['currency_pair', 'created_at']
