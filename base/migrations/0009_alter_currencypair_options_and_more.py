# Generated by Django 4.2.7 on 2023-11-21 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0008_currencypair_created_at"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="currencypair", options={"ordering": ["currency_pair", "created_at"]},
        ),
        migrations.AlterField(
            model_name="currencypair",
            name="currency_pair",
            field=models.CharField(max_length=20),
        ),
    ]
