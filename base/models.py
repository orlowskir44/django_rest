from django.db import models


class Currency(models.Model):
    code = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.code


class CurrencyPair(models.Model):
    currency_pair = models.CharField(max_length=20, unique=False)
    exchange_rate = models.FloatField(null=True, blank=True)
    created_at = models.DateField()

    class Meta:
        ordering = ['currency_pair', 'created_at']

    def __str__(self):
        return f'{self.currency_pair} : {self.created_at}'
