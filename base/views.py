from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Currency, CurrencyPair
from .serializers import CurrencySerializer
from django.http import JsonResponse
from datetime import datetime
import yfinance as yf


# Routes:
@api_view(['GET'])
def getRoutes(request):
    routes = [
        'GET /currency/',
        'GET /currency/<base>/<target>/',
    ]
    return Response(routes)


# List of all currencies:
@api_view(['GET'])
def getCurrency(request):
    queryset = Currency.objects.all()
    serializer = CurrencySerializer(queryset, many=True)

    return JsonResponse(serializer.data, safe=False)


# List of all currency_pair, exchange_rate:
@api_view(['GET'])
def getCurrencyPair(request, **kwargs):
    base_currency_code = kwargs.get('base')
    target_currency_code = kwargs.get('target')
    if base_currency_code and target_currency_code:
        currency_pair = f'{base_currency_code}{target_currency_code}'
        try:
            currency = CurrencyPair.objects.filter(currency_pair=currency_pair).order_by('-id')[0]
            response_data = {"currency_pair": currency.currency_pair, "exchange_rate": currency.exchange_rate}

            return JsonResponse(response_data)

        except Exception as e:
            return JsonResponse({'error': str(e)})

    return JsonResponse({"error": "Invalid currency codes"})


# ADDITIONAL:

# GET CODE FROM YFINANCE
def getData(request, **kwargs):
    base_currency_code = kwargs.get('base')
    try:
        currency_pair = yf.Ticker(f'{base_currency_code}').info['symbol']

        Currency.objects.get_or_create(code=currency_pair)

        response_data = {"code": currency_pair}

        return JsonResponse(response_data)

    except Exception as e:
        return JsonResponse({'error': str(e)})


# GET PAIR, RATE FROM YFINANCE:
def getDataCP(request, **kwargs):
    base_currency_code = kwargs.get('base')
    target_currency_code = kwargs.get('target')
    try:
        currency_pair = yf.Ticker(f'{base_currency_code}{target_currency_code}=X')
        exchange_rate = round(currency_pair.info['ask'], 3)

        name = currency_pair.info['shortName'].replace('/', '')
        print(name)
        print(exchange_rate)

        CurrencyPair.objects.get_or_create(
            currency_pair=name,
            exchange_rate=exchange_rate,
            created_at=datetime.now().date()
        )
        # currency.exchange_rate = exchange_rate
        # currency.save()

        response_data = {"currency_pair": name, "exchange_rate": exchange_rate}  # round(exchange_rate,3)}

        return JsonResponse(response_data)

    except Exception as e:
        return JsonResponse({'error': str(e)})
