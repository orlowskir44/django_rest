from django.urls import path
from . import views

urlpatterns = [
    path('', views.getRoutes, name='get_routes'),
    path('currency/', views.getCurrency, name='currency'),
    path('currency/<str:base>/<str:target>/', views.getCurrencyPair, name='currency_bt'),
    path('get_data_cp/<str:base>/<str:target>/', views.getDataCP),
    path('get_data/<str:base>/', views.getData),
]
