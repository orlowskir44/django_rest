from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from base.models import Currency, CurrencyPair
import json


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()

        Currency.objects.create(code='EUR')
        Currency.objects.create(code='PLN')
        CurrencyPair.objects.create(currency_pair='EURPLN', exchange_rate=123, created_at='2023-01-01')

        self.response_currency = self.client.get(reverse('currency'))
        self.response_currencyPair = self.client.get(reverse('currency_bt', args=['EUR', 'PLN']))

        self.currency = Currency.objects.all()
        self.currency_expected_data = [{'code': currency.code} for currency in self.currency]

        self.currency_pair = CurrencyPair.objects.get(currency_pair='EURPLN')
        self.currency_pair_expected_data = \
            {'currency_pair': self.currency_pair.currency_pair, 'exchange_rate': self.currency_pair.exchange_rate}

    def test_currency_GET(self):
        self.assertEqual(self.response_currency.status_code, status.HTTP_200_OK)

    def test_currency_response(self):
        self.assertEqual(json.loads(self.response_currency.content), self.currency_expected_data)

    def test_currencyPair_GET(self):
        self.assertEquals(self.response_currencyPair.status_code, status.HTTP_200_OK)

    def test_currencyPair_response(self):
        self.assertEqual(json.loads(self.response_currencyPair.content), self.currency_pair_expected_data)

    def test_currency_len(self):
        self.assertEqual(len(self.currency), 2)
