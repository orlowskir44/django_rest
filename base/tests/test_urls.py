from django.test import SimpleTestCase
from django.urls import resolve, reverse
from base.views import getRoutes, getCurrency, getCurrencyPair


class TestUrls(SimpleTestCase):

    def test_getRoutes_url_is_resolved(self):
        url = reverse('get_routes')
        self.assertEquals(resolve(url).func, getRoutes)

    def test_getCurrency_url_is_resolved(self):
        url = reverse('currency')
        self.assertEquals(resolve(url).func, getCurrency)

    def test_getCurrencyPair_url_is_resolved(self):
        url = reverse('currency_bt', args=['PLN', 'USD'])
        self.assertEquals(resolve(url).func, getCurrencyPair)
