from rest_framework.serializers import ModelSerializer
from .models import Currency, CurrencyPair


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = ['code']


class CurrencyPairSerializer(ModelSerializer):
    class Meta:
        model = CurrencyPair
        fields = '__all__'
