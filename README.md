# Django_REST_API
<hr /> 

### Clone the project
```
git@gitlab.com:orlowskir44/django_rest.git
```
<hr />

## Quick Start
```
ROUTES:
GET /currency/
GET /currency/<base>/<target>/
```
## Additional:
### Get data from Yfinance
Retrieves the current data and writes it to the database
```
ROUTES:
GET get_data_cp/<str:base>/<str:target>/
GET get_data/<str:base>/
```
<hr /> 

### Admin
To speed up the task check, I provide the database and login information
```
login: admin 
password: adminadmin
```

